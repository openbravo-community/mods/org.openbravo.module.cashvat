/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2015-2021 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.cashvat.util;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.query.Query;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.invoice.InvoiceTax;
import org.openbravo.model.common.invoice.InvoiceTaxCashVAT;
import org.openbravo.module.cashvat.model.ManualSettlementCashVAT;
import org.openbravo.module.cashvat.model.ManualSettlementLineCashVAT;

public class OBCVAT_Utility {
  private static final Logger log = Logger.getLogger(OBCVAT_Utility.class);

  /**
   * Creates a new Manual Settlement Cash VAT line and saves it in DAL session (not flushed yet)
   */
  public static ManualSettlementLineCashVAT createManualSettlementLine(
      final ManualSettlementCashVAT manualSettlement, final Long lineNo,
      final InvoiceTax invoiceTax, final BigDecimal percentage, final BigDecimal taxAmount,
      final BigDecimal taxableAmount) {
    final ManualSettlementLineCashVAT manualSettlementLine = OBProvider.getInstance().get(
        ManualSettlementLineCashVAT.class);
    manualSettlementLine.setClient(invoiceTax.getClient());
    manualSettlementLine.setOrganization(invoiceTax.getOrganization());
    manualSettlementLine.setObcvatManualSettlement(manualSettlement);
    manualSettlementLine.setLineNo(lineNo);
    manualSettlementLine.setInvoice(invoiceTax.getInvoice());
    manualSettlementLine.setDocumentType(manualSettlementLine.getInvoice().getDocumentType());
    manualSettlementLine.setSalesTransaction(manualSettlementLine.getDocumentType()
        .isSalesTransaction());
    manualSettlementLine.setTax(invoiceTax.getTax());
    manualSettlementLine.setInvoiceTax(invoiceTax);
    manualSettlementLine.setPercentage(percentage);
    manualSettlementLine.setTaxAmount(taxAmount);
    manualSettlementLine.setTaxableAmount(taxableAmount);
    OBDal.getInstance().save(manualSettlementLine);

    if (log.isDebugEnabled()) {
      log.debug(String
          .format(
              "Created Manual Settlement Line for the Manual Settlement: %s, Line No: %d, Invoice Tax: %s, Percentage: %f, Tax Amount: %f and Tax Base Amount: %f",
              manualSettlement.getIdentifier(), lineNo, invoiceTax.getIdentifier(), percentage,
              taxAmount, taxableAmount));
    }
    return manualSettlementLine;
  }

  /**
   * Creates a new Invoice Tax Cash VAT from the Manual Settlement Cash VAT line (creating the link
   * between them) and saves it in DAL session (not flushed yet)
   */
  public static InvoiceTaxCashVAT createInvoiceTaxCashVAT(
      final ManualSettlementLineCashVAT manualSettlementLine) {
    final InvoiceTaxCashVAT invoiceTaxCashVAT = OBProvider.getInstance().get(
        InvoiceTaxCashVAT.class);
    invoiceTaxCashVAT.setClient(manualSettlementLine.getClient());
    invoiceTaxCashVAT.setOrganization(manualSettlementLine.getOrganization());
    invoiceTaxCashVAT.setInvoiceTax(manualSettlementLine.getInvoiceTax());
    invoiceTaxCashVAT.setManualSettlement(true);
    invoiceTaxCashVAT.setPercentage(manualSettlementLine.getPercentage());
    invoiceTaxCashVAT.setTaxAmount(manualSettlementLine.getTaxAmount());
    invoiceTaxCashVAT.setTaxableAmount(manualSettlementLine.getTaxableAmount());
    manualSettlementLine.setInvoiceTaxCashVAT(invoiceTaxCashVAT);
    OBDal.getInstance().save(invoiceTaxCashVAT);
    OBDal.getInstance().save(manualSettlementLine);

    return invoiceTaxCashVAT;
  }

  /**
   * Deletes all the lines from the given Manual Settlement Cash VAT.
   * 
   * Returns the number of records deleted
   */
  public static int deleteManualSettlementCashVATLines(final String manualSettlementId) {
    //@formatter:off
    String hql = ""
        + "delete from OBCVAT_ManualSettlementLine l "
        + " where "
        + " l.obcvatManualSettlement.id = :manualSettlementId ";
    //@formatter:on
    @SuppressWarnings("rawtypes")
    Query query = OBDal.getInstance().getSession().createQuery(hql);
    query.setParameter("manualSettlementId", manualSettlementId);

    int removedLines = query.executeUpdate();
    if (log.isDebugEnabled()) {
      log.debug("Removed lines: " + removedLines);
    }
    return removedLines;
  }

  /**
   * Deletes all the lines from the given Manual Settlement Cash VAT.
   * 
   * Returns the number of records deleted
   */
  public static int deleteInvoiceTaxCashVATLines(final String manualSettlementId) {
    //@formatter:off
    String hql = ""
        + " delete from InvoiceTaxCashVAT itcv "
        + " where "
        + " exists (from OBCVAT_ManualSettlementLine msl "
        + "         where msl.obcvatManualSettlement.id = :manualSettlementId "
        + "         and itcv.id = msl.invoiceTaxCashVAT.id ) "
        + " and itcv.isManualSettlement = true ";
    //@formatter:on
    @SuppressWarnings("rawtypes")
    Query query = OBDal.getInstance().getSession().createQuery(hql);
    query.setParameter("manualSettlementId", manualSettlementId);

    int removedLines = query.executeUpdate();
    if (log.isDebugEnabled()) {
      log.debug("Removed lines: " + removedLines);
    }
    return removedLines;
  }

  /**
   * Returns a JSONObject with the message to be printed
   */
  public static JSONObject getResultMessage(final String msgType, final String msgTitle,
      final String msgText) {
    final JSONObject result = new JSONObject();
    try {
      final JSONArray actions = new JSONArray();
      final JSONObject msgInBPTab = new JSONObject();
      msgInBPTab.put("msgType", msgType);
      msgInBPTab.put("msgTitle", msgTitle);
      msgInBPTab.put("msgText", msgText);
      final JSONObject msgInBPTabAction = new JSONObject();
      msgInBPTabAction.put("showMsgInView", msgInBPTab);
      actions.put(msgInBPTabAction);
      result.put("responseActions", actions);
    } catch (Exception e) {
      log.error(e);
    }

    return result;
  }

}
