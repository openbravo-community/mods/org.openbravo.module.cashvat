/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2015 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.cashvat.hqlquerytransformer;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.module.cashvat.model.ManualSettlementCashVAT;
import org.openbravo.service.datasource.hql.HqlQueryTransformer;

@ComponentProvider.Qualifier("10D7A16FB3374057985C695365B2974F")
public class CashVATForSettlementTransformer extends HqlQueryTransformer {

  @Override
  public String transformHqlQuery(String hqlQuery, Map<String, String> requestParameters,
      Map<String, Object> queryNamedParameters) {
    String transformedHql = hqlQuery.replace("@whereClause@",
        getWhereClause(requestParameters, queryNamedParameters));
    transformedHql = transformedHql.replace("@selectClause@", " ");
    transformedHql = transformedHql.replace("@joinClause@", " ");
    transformedHql = transformedHql.replace("@groupby@", " ");
    transformedHql = transformedHql.replace("@having@", " ");

    final boolean isOrder = StringUtils.containsIgnoreCase(hqlQuery, "order by");
    transformedHql = transformedHql.replace("@orderby@",
        isOrder ? " " : getDefaultOrderByClause(requestParameters, queryNamedParameters));
    return transformedHql;
  }

  protected String getWhereClause(Map<String, String> requestParameters,
      Map<String, Object> queryNamedParameters) {
    final StringBuffer whereClause = new StringBuffer();

    final String manualSettlementId = requestParameters.get("@OBCVAT_ManualSettlement.id@");
    if (StringUtils.isNotBlank(manualSettlementId)) {
      try {
        OBContext.setAdminMode(true);
        final ManualSettlementCashVAT manualSettlement = OBDal.getInstance().get(
            ManualSettlementCashVAT.class, manualSettlementId);
        final Date startDate = manualSettlement.getStartingDate();
        final Date endDate = manualSettlement.getEndingDate();
        final Organization org = manualSettlement.getOrganization();
        final String currencyId = manualSettlement.getCurrency().getId();

        whereClause.append(" and i.invoiceDate >= :startDate ");
        whereClause.append(" and i.invoiceDate <= :endDate ");
        whereClause.append(" and AD_ISORGINCLUDED(i.organization.id, :orgId, :clientId) <> -1 ");
        whereClause.append(" and i.currency.id = :currencyId ");

        queryNamedParameters.put("startDate", startDate);
        queryNamedParameters.put("endDate", endDate);
        queryNamedParameters.put("orgId", org.getId());
        queryNamedParameters.put("clientId", org.getClient().getId());
        queryNamedParameters.put("currencyId", currencyId);
        // From the HQL table definition
        queryNamedParameters.put("thisManualSettlement", manualSettlementId);
      } finally {
        OBContext.restorePreviousMode();
      }
    }

    return whereClause.toString();
  }

  protected String getDefaultOrderByClause(Map<String, String> requestParameters,
      Map<String, Object> queryNamedParameters) {
    return "order by i.documentNo asc, i.invoiceDate asc ";
  }

}
