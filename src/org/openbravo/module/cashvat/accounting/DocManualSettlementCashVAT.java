/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2015-2021 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.cashvat.accounting;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.ad_forms.DocLine;
import org.openbravo.erpCommon.ad_forms.Fact;
import org.openbravo.erpCommon.utility.CashVATUtil;
import org.openbravo.erpCommon.utility.FieldProviderFactory;
import org.openbravo.erpCommon.utility.OBDateUtils;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.common.enterprise.AcctSchemaTableDocType;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceTaxCashVAT_V;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchemaTable;
import org.openbravo.module.cashvat.model.ManualSettlementCashVAT;
import org.openbravo.module.cashvat.model.ManualSettlementLineCashVAT;

public class DocManualSettlementCashVAT extends AcctServer {

  private static Logger log4j = Logger.getLogger(DocManualSettlementCashVAT.class);

  @Override
  public void loadObjectFieldProvider(ConnectionProvider conn, String _AD_Client_ID, String Id)
      throws ServletException {
    final ManualSettlementCashVAT ms = OBDal.getInstance().get(ManualSettlementCashVAT.class, Id);
    FieldProviderFactory[] data = new FieldProviderFactory[1];
    data[0] = new FieldProviderFactory(null);
    FieldProviderFactory.setField(data[0], "AD_Client_ID", ms.getClient().getId());
    FieldProviderFactory.setField(data[0], "AD_Org_ID", ms.getOrganization().getId());
    FieldProviderFactory.setField(data[0], "DocumentNo", ms.getDocumentNo());
    final String strAcctDate = OBDateUtils.formatDate(ms.getAccountingDate());
    FieldProviderFactory.setField(data[0], "DateAcct", strAcctDate);
    FieldProviderFactory.setField(data[0], "DateDoc", strAcctDate);
    FieldProviderFactory.setField(data[0], "C_DocType_ID", ms.getDocumentType().getId());
    FieldProviderFactory.setField(data[0], "C_Currency_ID", ms.getCurrency().getId());
    FieldProviderFactory.setField(data[0], "Description", ms.getDescription());
    FieldProviderFactory.setField(data[0], "Posted", ms.getPosted());
    FieldProviderFactory.setField(data[0], "Processed", ms.isProcessed() ? "Y" : "N");
    FieldProviderFactory.setField(data[0], "Processing", ms.isProcessNow() ? "Y" : "N");

    setObjectFieldProvider(data);
  }

  @Override
  public boolean loadDocumentDetails(FieldProvider[] data, ConnectionProvider conn) {
    loadDocumentType();
    p_lines = loadLines();
    return true;
  }

  @Override
  public BigDecimal getBalance() {
    return BigDecimal.ZERO;
  }

  @Override
  public Fact createFact(AcctSchema as, ConnectionProvider conn, Connection con,
      VariablesSecureApp vars) throws ServletException {
    Fact fact = new Fact(this, as, Fact.POST_Actual);
    String Fact_Acct_Group_ID = SequenceIdData.getUUID();
    String SeqNo = "0";

    try {
      OBContext.setAdminMode(true);
      /* Template support */
      DocManualSettlementCashVATTemplate docManualSettlementCashVATTemplate = getDocManualSettlementCashVATTemplate(as);
      if (docManualSettlementCashVATTemplate != null) {
        return docManualSettlementCashVATTemplate.createFact(this, as, conn, con, vars);
      }

      /* Default engine */
      for (int i = 0; p_lines != null && i < p_lines.length; i++) {
        final DocLine_ManualSettlement line = (DocLine_ManualSettlement) p_lines[i];
        SeqNo = CashVATUtil.createFactCashVAT(as, conn, fact, Fact_Acct_Group_ID, line,
            line.getInvoice(), DocumentType, SeqNo);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return fact;
  }

  private DocManualSettlementCashVATTemplate getDocManualSettlementCashVATTemplate(AcctSchema as) {
    String strClassname = "";

    //@formatter:off
    String whereClause = ""
        + " as astdt "
        + " where astdt.acctschemaTable.accountingSchema.id = :acctSchemaId "
        + " and astdt.acctschemaTable.table.id = :tableId "
        + " and astdt.documentCategory = :documentType ";
    //@formatter:on
    final OBQuery<AcctSchemaTableDocType> obqParameters = OBDal.getInstance()
        .createQuery(AcctSchemaTableDocType.class, whereClause);
    obqParameters.setNamedParameter("acctSchemaId", as.m_C_AcctSchema_ID);
    obqParameters.setNamedParameter("tableId", AD_Table_ID);
    obqParameters.setNamedParameter("documentType", DocumentType);
    obqParameters.setMaxResult(1);
    final List<AcctSchemaTableDocType> acctSchemaTableDocTypes = obqParameters.list();
    if (acctSchemaTableDocTypes != null && !acctSchemaTableDocTypes.isEmpty()) {
      strClassname = acctSchemaTableDocTypes.get(0).getCreatefactTemplate().getClassname();
    }

    if (StringUtils.isBlank(strClassname)) {
      //@formatter:off
      String whereClause2 = ""
          + " as ast "
          + " where ast.accountingSchema.id = :acctSchemaId "
          + " and ast.table.id = :tableId ";
      //@formatter:on

      final OBQuery<AcctSchemaTable> obqParameters2 = OBDal.getInstance()
          .createQuery(AcctSchemaTable.class, whereClause2);
      obqParameters2.setNamedParameter("acctSchemaId", as.m_C_AcctSchema_ID);
      obqParameters2.setNamedParameter("tableId", AD_Table_ID);
      obqParameters2.setMaxResult(1);

      final List<AcctSchemaTable> acctSchemaTables = obqParameters2.list();
      if (acctSchemaTables != null && !acctSchemaTables.isEmpty()
          && acctSchemaTables.get(0).getCreatefactTemplate() != null)
        strClassname = acctSchemaTables.get(0).getCreatefactTemplate().getClassname();
    }

    if (StringUtils.isNotBlank(strClassname)) {
      try {
        return (DocManualSettlementCashVATTemplate) Class.forName(strClassname).getDeclaredConstructor().newInstance();
      } catch (Exception e) {
        log4j.error("Error while creating new instance for DocManualSettlementCashVATTemplate - ",
            e);
      }
    }
    return null;
  }

  @Override
  public boolean getDocumentConfirmation(ConnectionProvider conn, String strRecordId) {
    return true;
  }

  private FieldProviderFactory[] loadLinesFieldProvider() {
    ScrollableResults mslScroller = null;
    final List<FieldProviderFactory> fpList = new ArrayList<FieldProviderFactory>();
    try {
      OBContext.setAdminMode(true);

      final String invoiceDocumentno = OBMessageUtils.messageBD("InvoiceDocumentno");
      final ManualSettlementCashVAT ms = OBDal.getInstance().get(ManualSettlementCashVAT.class,
          Record_ID);
      final Date msDate = ms.getAccountingDate();

      final OBQuery<ManualSettlementLineCashVAT> mslQuery = OBDal.getInstance().createQuery(
          ManualSettlementLineCashVAT.class,
          "obcvatManualSettlement.id = :obcvatManualSettlementId");
      mslQuery.setFetchSize(1000);
      mslQuery.setNamedParameter("obcvatManualSettlementId", Record_ID);
      mslScroller = mslQuery.scroll(ScrollMode.FORWARD_ONLY);
      int i = 1;
      while (mslScroller.next()) {
        final ManualSettlementLineCashVAT msl = (ManualSettlementLineCashVAT) mslScroller.get(0);
        final FieldProviderFactory fp = new FieldProviderFactory(msl);
        final Invoice invoice = msl.getInvoice();

        FieldProviderFactory.setField(fp, "adOrgId", msl.getOrganization().getId());
        FieldProviderFactory.setField(fp, "description", invoice.getDescription());
        FieldProviderFactory.setField(fp, "cCurrencyId", invoice.getCurrency().getId());
        FieldProviderFactory.setField(fp, "cInvoiceId", invoice.getId());
        FieldProviderFactory.setField(fp, "description",
            invoiceDocumentno + " " + invoice.getDocumentNo());
        FieldProviderFactory.setField(fp, "dateacct", OBDateUtils.formatDate(msDate));
        FieldProviderFactory.setField(fp, "datedoc", fp.getField("dateacct"));
        FieldProviderFactory.setField(fp, "cTaxId", msl.getTax().getId());

        FieldProviderFactory.setField(fp, "OBCVAT_MANUALSETTLEMENT_ID", msl
            .getObcvatManualSettlement().getId());
        FieldProviderFactory.setField(fp, "OBCVAT_MANUALSETTLEMENTLINE_ID", msl.getId());
        FieldProviderFactory
            .setField(fp, "InvoiceTaxCashVAT_V", msl.getInvoiceTaxCashVAT().getId());

        FieldProviderFactory.setField(fp, "cBpartnerId", invoice.getBusinessPartner().getId());
        FieldProviderFactory.setField(fp, "cProjectId", invoice.getProject() != null ? invoice
            .getProject().getId() : "");
        FieldProviderFactory.setField(fp, "cCampaignId",
            invoice.getSalesCampaign() != null ? invoice.getSalesCampaign().getId() : "");

        fpList.add(fp);

        if ((i % 100) == 0) {
          OBDal.getInstance().getSession().clear();
        }
        i++;
      }
    } finally {
      if (mslScroller != null) {
        mslScroller.close();
      }
      OBContext.restorePreviousMode();
    }

    FieldProviderFactory[] data = new FieldProviderFactory[fpList.size()];
    data = fpList.toArray(data);

    return data;
  }

  private DocLine[] loadLines() {
    ArrayList<Object> list = new ArrayList<Object>();
    FieldProviderFactory[] data = loadLinesFieldProvider();
    if (data == null || data.length == 0) {
      return null;
    }
    for (int i = 0; i < data.length; i++) {
      if (data[i] == null) {
        continue;
      }
      try {
        OBContext.setAdminMode(true);
        final String lineID = data[i].getField("OBCVAT_MANUALSETTLEMENTLINE_ID");
        final DocLine_ManualSettlement docLine = new DocLine_ManualSettlement(DocumentType,
            Record_ID, lineID);
        docLine.loadAttributes(data[i], this);
        docLine.setInvoiceId(data[i].getField("cInvoiceId"));
        final InvoiceTaxCashVAT_V itcv = (InvoiceTaxCashVAT_V) OBDal.getInstance().getProxy(
            InvoiceTaxCashVAT_V.ENTITY_NAME, data[i].getField("InvoiceTaxCashVAT_V"));
        final List<String> itcvL = new ArrayList<String>(1);
        itcvL.add(itcv.getId());
        docLine.setInvoiceTaxCashVAT_V_IDs(itcvL);
        list.add(docLine);
      } finally {
        OBContext.restorePreviousMode();
      }
    }
    DocLine_ManualSettlement[] dl = new DocLine_ManualSettlement[list.size()];
    list.toArray(dl);
    return dl;
  }
}
