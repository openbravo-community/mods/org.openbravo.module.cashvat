/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2015 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.cashvat.handler;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.module.cashvat.model.ManualSettlementCashVAT;
import org.openbravo.module.cashvat.util.OBCVAT_Utility;
import org.openbravo.service.db.DbUtility;

/**
 * Reactivates a Manual Cash VAT Settlement. User can select to delete the settlement lines or not
 * 
 */
public class ReactivateCashVATManualSettlementHandler extends BaseProcessActionHandler {
  private static final Logger log = Logger
      .getLogger(ReactivateCashVATManualSettlementHandler.class);

  public static final String ACTION_REACTIVATE = "RE";
  public static final String ACTION_REACTIVATE_DELETE = "RD";
  public static final String STATUS_REACTIVATE = "DR";

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String data) {
    JSONObject result = new JSONObject();
    ManualSettlementCashVAT manualSettlement = null;
    try {
      OBContext.setAdminMode(true);

      final JSONObject jsonData = new JSONObject(data);
      final JSONObject jsonparams = jsonData.getJSONObject("_params");

      final String manualSettlementId = jsonData.getString("inpobcvatManualsettlementId");
      final String action = jsonparams.getString("action");
      if (!(StringUtils.equals(ACTION_REACTIVATE, action) || StringUtils.equals(
          ACTION_REACTIVATE_DELETE, action))) {
        throw new UnsupportedOperationException("The action " + action
            + " is not supported by ReactivateCashVATManualSettlementHandler class");
      }

      manualSettlement = OBDal.getInstance().get(ManualSettlementCashVAT.class, manualSettlementId);

      if (StringUtils.equals(manualSettlement.getPosted(), "Y")) {
        throw new OBException("@DocumentPosted@");
      }

      // Update the manual settlement header

      manualSettlement.setProcessed(false);
      manualSettlement.setDocumentStatus(STATUS_REACTIVATE);
      OBDal.getInstance().save(manualSettlement);
      OBDal.getInstance().getConnection(true).commit();

      // Delete the Invoice Tax Cash VAT lines set to null its reference to the manual settlement
      // line
      OBCVAT_Utility.deleteInvoiceTaxCashVATLines(manualSettlementId);

      // Delete too the manual settlement lines
      if (StringUtils.equals(ACTION_REACTIVATE_DELETE, action)) {
        OBCVAT_Utility.deleteManualSettlementCashVATLines(manualSettlementId);
      }

      if (StringUtils.equals(ACTION_REACTIVATE_DELETE, action)) {
        manualSettlement.setSalesTaxableAmount(null);
        manualSettlement.setSalesTaxAmount(null);
        manualSettlement.setPurchaseTaxableAmount(null);
        manualSettlement.setPurchaseTaxAmount(null);
      }
      OBDal.getInstance().save(manualSettlement);
      OBDal.getInstance().flush();

      // Success Message
      result = OBCVAT_Utility.getResultMessage("success", OBMessageUtils.messageBD("Done"),
          OBMessageUtils.messageBD("Success"));
    } catch (Exception e) {
      OBDal.getInstance().getSession().getTransaction().rollback();
      try {
        if (manualSettlement != null) {
          OBDal.getInstance().refresh(manualSettlement);
          manualSettlement.setProcessed(true);
          manualSettlement.setDocumentStatus(CashVATManualSettlementHandler.STATUS_COMPLETE);
          OBDal.getInstance().save(manualSettlement);
          OBDal.getInstance().getConnection(true).commit();
        }

        log.error(e);
        Throwable ex = DbUtility.getUnderlyingSQLException(e);
        String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
        result = OBCVAT_Utility.getResultMessage("error", OBMessageUtils.messageBD("Error"),
            message);
      } catch (Exception e1) {
        throw new OBException(e1);
      }
    } finally {
      OBContext.restorePreviousMode();
    }

    return result;
  }
}
