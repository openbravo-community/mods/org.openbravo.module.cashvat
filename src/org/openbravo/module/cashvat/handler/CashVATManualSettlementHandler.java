/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2015 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.cashvat.handler;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.invoice.InvoiceTax;
import org.openbravo.module.cashvat.model.ManualSettlementCashVAT;
import org.openbravo.module.cashvat.model.ManualSettlementLineCashVAT;
import org.openbravo.module.cashvat.util.OBCVAT_Utility;
import org.openbravo.service.db.DbUtility;

/**
 * Creates the Manual Settlement lines from the selected records. It first deletes all the lines and
 * then created the new ones based on the P&E selection
 */
public class CashVATManualSettlementHandler extends BaseProcessActionHandler {
  private static final Logger log = Logger.getLogger(CashVATManualSettlementHandler.class);

  public static final String ACTION_OK = "OK";
  public static final String ACTION_COMPLETE = "CO";
  public static final String STATUS_COMPLETE = "CO";
  public static final String STATUS_DRAFT = "DR";

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String data) {
    JSONObject result = new JSONObject();
    try {
      OBContext.setAdminMode(true);

      final JSONObject jsonData = new JSONObject(data);
      final JSONObject jsonparams = jsonData.getJSONObject("_params");

      final JSONArray selectedLines = jsonparams.getJSONObject("lines").getJSONArray("_selection");
      final String manualSettlementId = jsonData.getString("inpobcvatManualsettlementId");
      final String action = jsonData.getString("_buttonValue");

      if (!(StringUtils.equals(ACTION_OK, action) || StringUtils.equals(ACTION_COMPLETE, action))) {
        throw new UnsupportedOperationException("The action " + action
            + " is not supported by CashVATManualSettlementHandler class");
      }

      int selectedInvoiceLinesLength = selectedLines.length();
      if (selectedInvoiceLinesLength == 0 && StringUtils.equals(ACTION_COMPLETE, action)) {
        throw new OBException("@NotSelected@");
      }

      // First delete the previous lines
      OBCVAT_Utility.deleteManualSettlementCashVATLines(manualSettlementId);

      // Create the lines based on the selection
      final ManualSettlementCashVAT manualSettlement = (ManualSettlementCashVAT) OBDal
          .getInstance().getProxy(ManualSettlementCashVAT.ENTITY_NAME, manualSettlementId);
      BigDecimal taxAmountTotalPurchase = BigDecimal.ZERO;
      BigDecimal taxableAmountTotalPurchase = BigDecimal.ZERO;
      BigDecimal taxAmountTotalSales = BigDecimal.ZERO;
      BigDecimal taxableAmountTotalSales = BigDecimal.ZERO;
      for (int i = 0; i < selectedInvoiceLinesLength; i++) {
        final JSONObject lineJS = selectedLines.getJSONObject(i);
        final String invoiceTaxId = lineJS.getString("id");
        final BigDecimal percentage = new BigDecimal(lineJS.getString("percentage"));
        BigDecimal taxAmount = new BigDecimal(lineJS.getString("taxAmount"));
        BigDecimal taxableAmount = new BigDecimal(lineJS.getString("taxableAmount"));
        final InvoiceTax invoiceTax = OBDal.getInstance().get(InvoiceTax.class, invoiceTaxId);

        final ManualSettlementLineCashVAT manualSettlementLine = OBCVAT_Utility
            .createManualSettlementLine(manualSettlement, Long.valueOf((i + 1) * 10), invoiceTax,
                percentage, taxAmount, taxableAmount);
        // Create the Invoice Tax Cash VAT in case of complete
        if (StringUtils.equals(ACTION_COMPLETE, action)) {
          OBCVAT_Utility.createInvoiceTaxCashVAT(manualSettlementLine);
        }
        final DocumentType docType = manualSettlementLine.getInvoice().getDocumentType();
        if (docType.isReversal()) {
          taxAmount = taxAmount.negate();
          taxableAmount = taxableAmount.negate();
        }
        if (manualSettlementLine.isSalesTransaction()) {
          taxAmountTotalSales = taxAmountTotalSales.add(taxAmount);
          taxableAmountTotalSales = taxableAmountTotalSales.add(taxableAmount);
        } else {
          taxAmountTotalPurchase = taxAmountTotalPurchase.add(taxAmount);
          taxableAmountTotalPurchase = taxableAmountTotalPurchase.add(taxableAmount);
        }

        if (i > 0 && (i % 100) == 0) {
          OBDal.getInstance().flush();
          OBDal.getInstance().getSession().clear();
        }
      }

      // Update totals in header
      OBDal.getInstance().refresh(manualSettlement);
      if (selectedInvoiceLinesLength == 0) {
        manualSettlement.setSalesTaxAmount(null);
        manualSettlement.setSalesTaxableAmount(null);
        manualSettlement.setPurchaseTaxAmount(null);
        manualSettlement.setPurchaseTaxableAmount(null);
      } else {
        manualSettlement
            .setSalesTaxAmount(taxAmountTotalSales.compareTo(BigDecimal.ZERO) != 0 ? taxAmountTotalSales
                : null);
        manualSettlement
            .setSalesTaxableAmount(taxableAmountTotalSales.compareTo(BigDecimal.ZERO) != 0 ? taxableAmountTotalSales
                : null);
        manualSettlement
            .setPurchaseTaxAmount(taxAmountTotalPurchase.compareTo(BigDecimal.ZERO) != 0 ? taxAmountTotalPurchase
                : null);
        manualSettlement.setPurchaseTaxableAmount(taxableAmountTotalPurchase
            .compareTo(BigDecimal.ZERO) != 0 ? taxableAmountTotalPurchase : null);
      }

      // Set as processed
      if (StringUtils.equals(ACTION_COMPLETE, action)) {
        manualSettlement.setProcessed(true);
        manualSettlement.setDocumentStatus(STATUS_COMPLETE);
      }

      OBDal.getInstance().save(manualSettlement);
      OBDal.getInstance().flush();

      // Success Message
      result = OBCVAT_Utility.getResultMessage("success", OBMessageUtils.messageBD("Done"),
          OBMessageUtils.messageBD("Success"));
    } catch (Exception e) {
      OBDal.getInstance().getSession().getTransaction().rollback();
      try {
        log.error(e);
        Throwable ex = DbUtility.getUnderlyingSQLException(e);
        String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
        result = OBCVAT_Utility.getResultMessage("error", OBMessageUtils.messageBD("Error"),
            message);
      } catch (Exception e1) {
        throw new OBException(e1);
      }
    } finally {
      OBContext.restorePreviousMode();
    }

    return result;
  }

}
