/************************************************************************************ 
 * Copyright (C) 2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/

package org.openbravo.module.cashvat.modulescript;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.database.ConnectionProvider;
import org.openbravo.modulescript.ModuleScript;
import org.openbravo.modulescript.ModuleScriptExecutionLimits;
import org.openbravo.modulescript.OpenbravoVersion;

public class CreateAccountingConfiguration extends ModuleScript {
  private static final Logger log4j = Logger.getLogger(CreateAccountingConfiguration.class);
  @Override
  // Inserting:
  // 1) accounting schema tables for new tables in the module
  // 2) Period control for newly added DocBaseTypes
  public void execute() {
    try {
      ConnectionProvider cp = getConnectionProvider();
      createAcctSchemaTables(cp);
      createPeriodControl(cp);
    } catch (Exception e) {
      handleError(e);
    }
  }


  private void createAcctSchemaTables(ConnectionProvider cp) throws Exception {
    CreateAccountingConfigurationData[] data = CreateAccountingConfigurationData.selectAcctSchema(cp);
    final String TABLE_OBCVAT_MANUALSETTLEMENT = "C49E71C53DCB4061AA9CD728BA358083";
    int count = 0;
    for (int i = 0; i < data.length; i++) {
      boolean existInAcctSchema = CreateAccountingConfigurationData.selectTables(cp, data[i].cAcctschemaId, TABLE_OBCVAT_MANUALSETTLEMENT);
      if(!existInAcctSchema){
        CreateAccountingConfigurationData.insertAcctSchemaTable(cp.getConnection(), cp, data[i].cAcctschemaId, TABLE_OBCVAT_MANUALSETTLEMENT, data[i].adClientId);
        count++;
      }
    }
    if (count > 0) {
	  log4j.info("Created " + count + " accounting configuration table data");
	}
  }

  private void createPeriodControl(ConnectionProvider cp) throws Exception{
    int count = CreateAccountingConfigurationData.insertPeriodControl(cp.getConnection(), cp);
    if (count > 0) {
	  log4j.info("Created " + count + " period control records");
	}
  }
  
  @Override
  protected ModuleScriptExecutionLimits getModuleScriptExecutionLimits() {
    return new ModuleScriptExecutionLimits("95CA76BB409646CC8D9B68CD311CB415", null, 
        new OpenbravoVersion(1,0,100));
  }
}
