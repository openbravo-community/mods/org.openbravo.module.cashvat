/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014-2017 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.module.cashvat.modulescript;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.modulescript.ModuleScript;

public class AddMissingCashVATPayments extends ModuleScript {
  private static final Logger log4j = Logger.getLogger(AddMissingCashVATPayments.class);

  @Override
  public void execute() {
    ConnectionProvider cp = null;
    Connection con = null;
    try {
      cp = getConnectionProvider();
      con = cp.getConnection();
      con.setAutoCommit(false);
      boolean isModuleScriptExecuted = AddMissingCashVATPaymentsData.isModuleScriptExecuted(cp);
      if (!isModuleScriptExecuted) {
        deleteCashVATInfoWithoutPaymentDetail(cp);
        deleteCashVATInfoPartialPaidCreditMemo(cp);
        createCashVATInfo(cp);
        AddMissingCashVATPaymentsData.createPreference(cp);
      }
    } catch (Exception e) {
      handleError(e);
    }
  }

  private void deleteCashVATInfoWithoutPaymentDetail(ConnectionProvider cp) throws Exception {
    int deleted = AddMissingCashVATPaymentsData.deleteCashVATInfoWithoutPaymentDetail(cp);
    if (deleted > 0) {
      log4j.info("Deleted Cash VAT Info lines without payment detail info: " + deleted);
    }
  }

  private void deleteCashVATInfoPartialPaidCreditMemo(ConnectionProvider cp) throws Exception {
    int deleted = AddMissingCashVATPaymentsData.deleteCashVATInfoPartialPaidCreditMemo(cp);
    if (deleted > 0) {
      log4j.info("Deleted Cash VAT Info lines from partially paid credit memo: " + deleted);
    }
  }

  private void createCashVATInfo(ConnectionProvider cp) throws Exception {
    int totalMatched = 0;

    Map<String, String> invoiceLastPaymentDetailCache = new HashMap<String, String>();

    AddMissingCashVATPaymentsData data = null;
    try {    
      data = AddMissingCashVATPaymentsData.selectMissedPaymentDetails(cp);

      Connection con = cp.getConnection();
      con.setAutoCommit(false);
      int i = 0;
      while (data.next()) {
        AddMissingCashVATPaymentsData missedFPDs = data.get();
        final String clientId = missedFPDs.adClientId;
        final String orgId = missedFPDs.adOrgId;
        final String invoiceId = missedFPDs.cInvoiceId;
        final String isPaid = missedFPDs.ispaid;
        final String invoiceTaxId = missedFPDs.cInvoicetaxId;
        final String finPaymentDetailId = missedFPDs.finPaymentDetailId;
        final String stdPrecision = missedFPDs.stdprecision;
        final String percentage = missedFPDs.percentage;
        final String totalTaxAmt = missedFPDs.taxamt;
        final String totalTaxBaseAmt = missedFPDs.taxbaseamt;
  
        boolean isFinalAdjustment = false;
        if ("Y".equals(isPaid)) {
          String lastPaymentDetailId = invoiceLastPaymentDetailCache.get(invoiceId);
          if (lastPaymentDetailId == null) {
            lastPaymentDetailId = AddMissingCashVATPaymentsData
                .selectLastPaymentDetail(cp, invoiceId)[0].finPaymentDetailId;
            invoiceLastPaymentDetailCache.put(invoiceId, lastPaymentDetailId);
          }
  
          if (finPaymentDetailId.equals(lastPaymentDetailId)) {
            isFinalAdjustment = true;
          }
        }
  
        if (isFinalAdjustment) {
          AddMissingCashVATPaymentsData[] calculatedDataLastPayment = AddMissingCashVATPaymentsData
              .calculateLastPaymentInfo(cp, totalTaxAmt, totalTaxBaseAmt, invoiceTaxId);
          totalMatched = totalMatched
              + AddMissingCashVATPaymentsData.insertCashVATInfoLastPayment(cp, clientId, orgId,
                  invoiceTaxId, calculatedDataLastPayment[0].percentage,
                  calculatedDataLastPayment[0].taxamt, calculatedDataLastPayment[0].taxbaseamt,
                  finPaymentDetailId);
        } else {
          totalMatched = totalMatched
              + AddMissingCashVATPaymentsData.insertCashVATInfoNonLastPayment(cp, clientId, orgId,
                  invoiceTaxId, percentage, totalTaxAmt, stdPrecision, totalTaxBaseAmt,
                  finPaymentDetailId);
        }
  
        if (++i % 100 == 0) {
          log4j.info("Records inserted: " + i + " ...");
          con.setAutoCommit(false);
          con.commit();
        }
      }
      con.setAutoCommit(false);
      con.commit();
      con.setAutoCommit(true);
  
      if (totalMatched > 0) {
        log4j.info("Fixed Invoice Tax Cash VAT lines: " + totalMatched);
      }
    } finally {
      if (data != null){
        data.close();
      }
    }
  }
}
