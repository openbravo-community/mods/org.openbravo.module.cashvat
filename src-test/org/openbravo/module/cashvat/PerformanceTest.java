package org.openbravo.module.cashvat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;
import org.openbravo.advpaymentmngt.dao.AdvPaymentMngtDao;
import org.openbravo.base.weld.test.WeldBaseTest;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.InvoiceLineTax;
import org.openbravo.model.ad.ui.Process;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.invoice.InvoiceTax;
import org.openbravo.service.db.CallProcess;

public class PerformanceTest extends WeldBaseTest {
  private static final String C_INVOICE_POST = "111";
  private static final int NEW_INVOICES_NUMBER = 100000;
  private static final String PREFIX = "AUTO_";

  // It must be in draft status
  private static final String BASE_INVOICE_ID = "93CA883DE3824E1580AECB51B6022B26";

  @Test
  public void testCreateNotPaidInvoices() throws Exception {
    setTestAdminContext();

    final Invoice invoice = OBDal.getInstance().get(Invoice.class, BASE_INVOICE_ID);
    for (int i = 0; i < NEW_INVOICES_NUMBER; i++) {
      Invoice newInvoice = (Invoice) DalUtil.copy(invoice, true);
      newInvoice.setDocumentNo(PREFIX + newInvoice.getDocumentNo() + "_" + i);
      newInvoice.setCreationDate(new Date());

      // Avoid problems with the triggers
      for (InvoiceLine il : newInvoice.getInvoiceLineList()) {
        il.setInvoiceLineTaxList(new ArrayList<InvoiceLineTax>());
      }
      newInvoice.setInvoiceLineTaxList(new ArrayList<InvoiceLineTax>(0));
      newInvoice.setInvoiceTaxList(new ArrayList<InvoiceTax>(0));
      newInvoice.setGrandTotalAmount(BigDecimal.ZERO);
      newInvoice.setSummedLineAmount(BigDecimal.ZERO);

      OBDal.getInstance().save(newInvoice);
      OBDal.getInstance().flush();

      // Process Invoice
      OBDal.getInstance().refresh(newInvoice);
      final String invoiceId = newInvoice.getId();
      Process process = new AdvPaymentMngtDao().getObject(Process.class, C_INVOICE_POST);
      CallProcess.getInstance().call(process, invoiceId, null);

      /* Adding a payment doesn't work properly, it's needed to change stuff in core */
      // Partially pay invoice
      // OBDal.getInstance().getConnection(true).commit();

      // OBDal.getInstance().flush();
      // OBDal.getInstance().refresh(newInvoice);
      //
      // List<FIN_PaymentScheduleDetail> scheduleDetails = newInvoice.getFINPaymentScheduleList()
      // .get(0).getFINPaymentScheduleDetailInvoicePaymentScheduleList();
      //
      // HashMap<String, BigDecimal> paidAmount = new HashMap<String, BigDecimal>();
      // final BigDecimal amount = newInvoice.getGrandTotalAmount().divide(BigDecimal.TEN,
      // RoundingMode.HALF_UP);
      // paidAmount.put(scheduleDetails.get(0).getId(), amount);
      //
      // final DocumentType docType = FIN_Utility.getDocumentType(newInvoice.getOrganization(),
      // newInvoice.isSalesTransaction() ? "ARR" : "APP");
      // final String docNo = FIN_Utility.getDocumentNo(newInvoice.getOrganization(),
      // docType.getDocumentCategory(), "FIN_Payment");
      //
      // final FIN_Payment payment = FIN_AddPayment
      // .savePayment(null, newInvoice.isSalesTransaction(), docType, docNo, newInvoice
      // .getBusinessPartner(), newInvoice.getPaymentMethod(),
      // newInvoice.isSalesTransaction() ? newInvoice.getBusinessPartner().getAccount()
      // : newInvoice.getBusinessPartner().getPOFinancialAccount(), amount.toString(),
      // new Date(), newInvoice.getOrganization(), null, scheduleDetails, paidAmount, false,
      // false);
      //
      // OBDal.getInstance().flush();
      // final String paymentId = payment.getId();
      // OBDal.getInstance().commitAndClose();
      //
      // FIN_PaymentProcess.doProcessPayment(OBDal.getInstance().get(FIN_Payment.class, paymentId),
      // "P", false, null, null);
      // // OBDal.getInstance().refresh(payment)
      // // OBDal.getInstance().save(payment);
      // OBDal.getInstance().flush();
      if (i % 100 == 0) {
        OBDal.getInstance().getConnection(true).commit();
        OBDal.getInstance().getSession().clear();
      }
    }

    OBDal.getInstance().getConnection(true).commit();
  }
}